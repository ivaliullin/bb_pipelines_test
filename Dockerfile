FROM golang:alpine AS build-env

ARG pgaddr
ENV PGHOST=$pgaddr
ADD . /go/src/bb_pipelines_test
WORKDIR /go/src/bb_pipelines_test
RUN echo $PGHOST
RUN apk update
RUN apk add git
RUN go get -v ./...
RUN go build -v

# final stage
FROM alpine
ENV TZ=Europe/Moscow
COPY --from=build-env /go/src/bb_pipelines_test/bb_pipelines_test /entrypoint
RUN apk update \
    && apk add tzdata \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENTRYPOINT ./entrypoint
